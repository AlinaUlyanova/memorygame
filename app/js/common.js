$(document).ready(function () {
	const cards = document.querySelectorAll('.memory-card');

	let hasFlippedCard, lock = false;
	let firstCard, secondCard;

	function flipCard() {
		if (lock) return;
		if (this === firstCard) return;
		this.classList.add('flip');

		if(!hasFlippedCard) {
			hasFlippedCard = true;
			firstCard = this;
			return;
		}

		secondCard = this;

		isEqual();
	}

	function isEqual() {
		let isMatch = firstCard.dataset.name === secondCard.dataset.name;
	    isMatch ? disableCards() : unflipCards();
	}

	function disableCards() {
		firstCard.removeEventListener('click', flipCard);
		secondCard.removeEventListener('click', flipCard);

		reset();
	}

	function unflipCards() {
		lock = true;
		setTimeout(() => {
			firstCard.classList.remove('flip');
			secondCard.classList.remove('flip');

			reset();
		}, 1000);
	}

	function reset() {
		[hasFlippedCard, lock] = [false, false];
		[firstCard, secondCard] = [null, null];
	}

	function shuffle() {
		let cards = $(".memory-card");
		for(var i = 0; i < cards.length; i++){
			let target = Math.floor(Math.random() * cards.length -1) + 1;
			let target2 = Math.floor(Math.random() * cards.length -1) +1;
			cards.eq(target).before(cards.eq(target2));
		}
	}

	$("#restart").click(function() {    
		cards.forEach(card => {
			if(card.classList.contains("flip")) 
				card.classList.remove('flip');
		});
		shuffle();
		cards.forEach(card => card.addEventListener('click', flipCard));
	});

	shuffle();
	cards.forEach(card => card.addEventListener('click', flipCard));
});